#include "stdafx.h"
#include <iostream>
#include <vector>
#include <Windows.h>
#include <functional>
#include <string>

using namespace std;

string formatRgbAnsi(string text, int r, int g, int b) {
	return "\033[38;2;" + to_string(r) + ";" +
		std::to_string(g) + ";" + std::to_string(b) +
		"m" + text + "\033[m";
}

struct figure {
	int x, y;
	figure(int newX, int newY) {
		x = newX;
		y = newY;
	}
};

vector<figure*> figures;

void show(int count) {
	cout << "Figures count->" << figures.size() << endl;
	cout << "Figures coordinates!" << endl;
	for (int figureIndex = 0; figureIndex < figures.size(); figureIndex++) {
		cout << formatRgbAnsi("Figure #", 100, 100, 100) << figureIndex + 1 << endl;
		cout << formatRgbAnsi("X: ", 0, 150, 30) << figures[figureIndex]->x << endl;
		cout << formatRgbAnsi("Y: ", 0, 150, 30) << figures[figureIndex]->y << endl;
	}

	cout << endl << "  ";
	for (int x = 1; x <= count; x++) {
		if(x < 10) cout << formatRgbAnsi(to_string(x), 0, 0, 180) << "|";
		else cout << formatRgbAnsi("*", 0, 0, 180) << "|";
	}
	cout << endl;
	
	for (int y = 1; y <= count; y++) {
		if (y < 10) cout << formatRgbAnsi(to_string(y), 0, 0, 180) << "|";
		else cout << formatRgbAnsi("*", 0, 0, 180) << "|";
		for (int x = 1; x <= count; x++) {
			bool isFigure = false;
			for (auto figuresFor : figures) {
				if (figuresFor->x == x && figuresFor->y == y)
					isFigure = true;
			}
			if (isFigure) {
				cout << formatRgbAnsi("x", 255, 100, 0) << "|";
			}
			else cout << "_|";
		}
		cout << endl;
	}
}

void logicEven(int count) {
	static bool theFirstPart = true,
				theFirstTime = true;
	int startY = count / 2 + 1;
	static int yHelper = 0;

	if (theFirstPart) {
		if (theFirstTime) {
			figures.push_back(new figure(1, startY + yHelper));
			theFirstTime = false;
		}

		if(figures.size() < count/2) {
			if ((figures[figures.size() - 1]->x + 2 != figures[figures.size() - 1]->y + 1) && (figures[figures.size() - 1]->x + 2 + figures[figures.size() - 1]->y + 1 != count + 1)) {
				if (figures[figures.size() - 1]->y + 1 > count) {
					figures.push_back(new figure(figures[figures.size() - 1]->x + 2, count - count/2 + 1));
				}
				else figures.push_back(new figure(figures[figures.size() - 1]->x + 2, figures[figures.size() - 1]->y + 1));
				return logicEven(count);
			}
			else {
				figures.clear();
				yHelper++;
				theFirstPart = true;
				theFirstTime = true;
				return logicEven(count);
			}
		}
		else {
			theFirstPart = false;
			return logicEven(count);
		}
	}
	else {
		for (int i = 0; i < count / 2; i++) {
			figures.push_back(new figure(count + 1 - figures[i]->x, count + 1 - figures[i]->y));
		}
	}

	yHelper = 0;
	theFirstPart = true;
	theFirstTime = true;
}

void logicNotEven(int count) {
	static int currentX = 1, currentY = 1;
	static bool isFirstTime = true;

	if (isFirstTime) {
		figures.push_back(new figure(1, 1));
		isFirstTime = false;
		currentX++;
	}

	if (figures.size() < count) {
		if (figures[figures.size() - 1]->y + 2 <= count) {
			figures.push_back(new figure(currentX, figures[figures.size() - 1]->y + 2));
			currentX++;
		}
		else {
			currentY++;
			figures.push_back(new figure(currentX, currentY));
			currentX++;
		}
		return logicNotEven(count);
	}

	isFirstTime = true;
	currentX = 1;
	currentY = 1;
}

void enter() {
	system("cls");
	int countOfCells;
	cout << formatRgbAnsi("Enter a field width: ", 200, 150, 0);
	cin >> countOfCells;

	if (!cin.good() || (countOfCells < 4 &&  countOfCells <= 4990)) {
		cout << formatRgbAnsi("Wrong number", 200, 0, 0) << endl;
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		countOfCells = 0;
		Sleep(500);
		return enter();
	}
	else {
		system("cls");
		if (countOfCells % 2 == 0) {
			logicEven(countOfCells);
		}
		else logicNotEven(countOfCells);
	}

	show(countOfCells);
}

int main()
{
	while (1) {
		enter();
		system("pause");
		figures.clear();
	}
		
    return 0;
}

